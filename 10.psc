Algoritmo ejercicio10
	nota <- 0
	Escribir 'Escribe la nota'
	Leer nota
	Si nota<5 Entonces
		Escribir 'Suspenso'
	SiNo
		Si nota=5 O nota=6 Entonces
			Escribir 'Suficiente'
		SiNo
			Si nota=6 O nota=7 Entonces
				Escribir 'Bien'
			SiNo
				Si nota=7 O nota=8 Entonces
					Escribir 'Notable'
				SiNo
					Escribir 'Sobresaliente'
				FinSi
			FinSi
		FinSi
	FinSi
FinAlgoritmo

